import React, { Component } from 'react';
import { connect } from 'react-redux';
import { trackEvent } from '@norse/consent-redux/actions';

class App extends Component {

  componentDidMount() {
    this.props.trackPageview();
  }

  render() {
    return (
      <div>
        <h1>Consent Redux example</h1>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  trackPageview() {
    dispatch(trackEvent('pageview'));
  }
});

export default connect(null, mapDispatchToProps)(App);
