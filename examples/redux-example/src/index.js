import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { trackingMiddleware } from '@norse/consent-redux';
import consentTracker from '@norse/consent-tracker';
import config from '../../config.js';

consentTracker.setConfig(config);

const store = createStore(()=>({}),
  applyMiddleware(
    trackingMiddleware(
      consentTracker
    )
  )
);

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>
, document.getElementById('root'));
