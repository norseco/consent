import React, { Component } from 'react';
import { withTracking } from '@norse/consent-react';

class App extends Component {

  componentDidMount() {
    this.props.tracker.trackEvent('pageview');
  }

  render() {
    return (
      <div>
        <h1>Consent React example</h1>
      </div>
    );
  }
}

export default withTracking(App);
