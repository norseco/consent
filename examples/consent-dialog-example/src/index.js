import './index.css';
import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { TrackingProvider } from '@norse/consent-react';
import consentTracker from '@norse/consent-tracker';
import config from '../../config.js';

consentTracker.setConfig(config);

ReactDOM.render(
  <TrackingProvider tracker={consentTracker}>
    <App />
  </TrackingProvider>,
  document.getElementById('root')
);
