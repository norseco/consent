import React, { Component } from 'react';
import { withTracking } from '@norse/consent-react';
import Dialog from './Dialog';

class App extends Component {

  componentDidMount() {
    this.props.tracker.trackEvent('pageview');
  }

  render() {
    return (
      <div>
        <h1>Consent dialog example</h1>
        <Dialog />
      </div>
    );
  }
}

export default withTracking(App);
