import './Dialog.css';
import React from 'react';
import { withConsent } from '@norse/consent-react';

const Dialog = ({accept, decline}) => (
  <div className="dialog">
    <div className="dialog__inner">
      <h3>Cookie Disclaimer</h3>
      <p>Varsity uses cookies and similar technologies on its website. By continuing your browsing after being presented with the cookie information you consent to such use.</p>
      <button className="btn" onClick={accept}>Accept</button>
      <button className="close" onClick={decline}>Decline</button>
    </div>
  </div>
);

export default withConsent(Dialog);
