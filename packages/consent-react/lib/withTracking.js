"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _TrackingProvider = require("./TrackingProvider");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var withTracking = function withTracking(Component) {
  return function (props) {
    return _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(_TrackingProvider.ConsentTrackerContext.Consumer, null, function (tracker) {
      return _react.default.createElement(Component, _extends({}, props, {
        tracker: tracker
      }));
    }));
  };
};

var _default = withTracking;
exports.default = _default;