import React from 'react';
import PropTypes from 'prop-types';

export const ConsentTrackerContext = React.createContext();

const TrackingProvider = ({ tracker, children }) => (
  <React.Fragment>
    <ConsentTrackerContext.Provider value={tracker}>
      {children}
    </ConsentTrackerContext.Provider>
  </React.Fragment>
);

TrackingProvider.propTypes = {
  children: PropTypes.any,
  tracker: PropTypes.object.isRequired,
}

export default TrackingProvider;
