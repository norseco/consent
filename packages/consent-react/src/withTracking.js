import React from 'react';
import { ConsentTrackerContext } from './TrackingProvider';

const withTracking = Component => props => (
  <React.Fragment>
    <ConsentTrackerContext.Consumer>
      {tracker => (
        <Component
          {...props}
          tracker={tracker}
        />
      )}
    </ConsentTrackerContext.Consumer>
  </React.Fragment>
);

export default withTracking;
