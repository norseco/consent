# Tracker

Library for configuring and handling tracking code across with user consent.

Click [here](https://bitbucket.org/norseco/consent/src/master/examples/ "Examples") for examples.

### Installation

```sh
$ npm install @norse/consent-tracker --save --registry https://npm.tools.norse.co
```

### Tracker

```javascript
import Tracker from '@norse/consent-tracker';
Tracker.setConfig(config);
```

```javascript
// Config example
{
    callbacks: {
        <providerName>: () => <code>
    },
    scripts: {
        <providerName>: <scriptUrl>
    },
    events: {
        <eventName>: [
            [<providerName>, <functionName>, <...arguments>],
            [<providerName2>, <functionName2>, <...arguments>]
        ]
    },
    scopes: {
        <providerName>: {
            dependencies: <providerName2>,
            requireConsent: true
        }
    }
}
```

```javascript
Tracker.addScript(<providerName>, <scriptUrl>);
```

```javascript
Tracker.trackEvent(<eventName>);
```

```javascript
Tracker.trackSingle(<providerName>, <functionName>, <...arguments>);
```
