export const ADD_SCRIPT = '@@Tracker/ADD_SCRIPT';
export const TRACK_EVENT = '@@Tracker/TRACK_EVENT';
export const TRACK_CUSTOM = '@@Tracker/TRACK_CUSTOM';

export const addScript = (label, url) => ({
  type: ADD_SCRIPT,
  label,
  url
});

export const trackEvent = (event, argObj) => ({
  type: TRACK_EVENT,
  event,
  argObj
});

export const trackCustom = (label, ...parameters) => ({
  type: TRACK_CUSTOM,
  label,
  parameters
});
