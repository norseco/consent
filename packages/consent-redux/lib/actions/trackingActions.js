"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.trackCustom = exports.trackEvent = exports.addScript = exports.TRACK_CUSTOM = exports.TRACK_EVENT = exports.ADD_SCRIPT = void 0;
var ADD_SCRIPT = '@@Tracker/ADD_SCRIPT';
exports.ADD_SCRIPT = ADD_SCRIPT;
var TRACK_EVENT = '@@Tracker/TRACK_EVENT';
exports.TRACK_EVENT = TRACK_EVENT;
var TRACK_CUSTOM = '@@Tracker/TRACK_CUSTOM';
exports.TRACK_CUSTOM = TRACK_CUSTOM;

var addScript = function addScript(label, url) {
  return {
    type: ADD_SCRIPT,
    label: label,
    url: url
  };
};

exports.addScript = addScript;

var trackEvent = function trackEvent(event, argObj) {
  return {
    type: TRACK_EVENT,
    event: event,
    argObj: argObj
  };
};

exports.trackEvent = trackEvent;

var trackCustom = function trackCustom(label) {
  for (var _len = arguments.length, parameters = new Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
    parameters[_key - 1] = arguments[_key];
  }

  return {
    type: TRACK_CUSTOM,
    label: label,
    parameters: parameters
  };
};

exports.trackCustom = trackCustom;