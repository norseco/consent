"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _actions = require("../actions");

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

var trackingMiddleware = function trackingMiddleware(tracker) {
  return function (store) {
    return function (next) {
      return function (action) {
        if (action && action.type) {
          switch (action.type) {
            case _actions.ADD_SCRIPT:
              tracker.addScript(action.label, action.url);
              break;

            case _actions.TRACK_EVENT:
              tracker.trackEvent(action.event, action.argObj);
              break;

            case _actions.TRACK_CUSTOM:
              tracker.trackSingle.apply(tracker, [action.label].concat(_toConsumableArray(action.parameters)));
              break;
          }

          next(action);
        }
      };
    };
  };
};

var _default = trackingMiddleware;
exports.default = _default;